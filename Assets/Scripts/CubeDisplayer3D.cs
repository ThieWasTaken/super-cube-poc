using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeDisplayer3D : CubeDisplayer
{
    protected override void UpdateDisplay()
    {
        rotation = transform.eulerAngles;
    }

    public void Twist(int Amount)
    {
        transform.Rotate(new Vector3(0, Amount, 0), Space.World);
        UpdateDisplay();
    }
}
