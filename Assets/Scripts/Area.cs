using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Area : MonoBehaviour
{
    public Dictionary<Vector2Int, GameObject> data;
    public GameObject cubePrefab;
    public CubeDisplayer CubeDisplayer;
    public EditMode mode = EditMode.Pause;
    public void PauseMode() => mode = EditMode.Pause;
    public void CreateMode() => mode = EditMode.Create;
    public void DeleteMode() => mode = EditMode.Delete;
    public ActionsHistory history;
    public void OnEnable()
    {
        if(data != null)
        {
            foreach (var item in data)
            {
                Destroy(item.Value);
            }
        }
        data = new Dictionary<Vector2Int, GameObject>();
        mode = EditMode.Create;
        history = new ActionsHistory();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                Debug.Log($"hitting: {hit.collider.name}");
                Vector2Int position = new Vector2Int((int)hit.point.x, (int)hit.point.z);
                switch (mode)
                {
                    case EditMode.Create:
                        Create(position);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    internal void Remove(Vector2Int position)
    {
        if (data.ContainsKey(position))
        {
            Destroy(data[position]);
        }
    }

    private void Create(Vector2Int position)
    {
        if(!data.ContainsKey(position))
        {
            var cube = CreateCube(position);
            history.Push(new CreatePlayerAction(position));
        }
        else
        {
            var oldCube = data[position];
            data.Remove(position);
            var newCube = CreateCube(position);
            history.Push(new ReplacePlayerAction(position, oldCube.transform.position, oldCube.transform.eulerAngles, cubePrefab));
            Destroy(oldCube);
        }
    }

    public GameObject CreateCube(Vector2Int position)
    {
        var cube = Instantiate(cubePrefab, transform);
        cube.transform.localScale = Vector3.one;
        cube.transform.eulerAngles = CubeDisplayer.rotation;
        cube.transform.position = new Vector3(position.x + 0.5f, transform.position.y + 0.5f, position.y + 0.5f);
        data[position] = cube;
        return cube;
    }

    public void Undo()
    {
        var pa = history.Pop();
        if(pa != null) pa.Execeute(this);
    }

    public void Redo()
    {
        
    }

    public enum EditMode
    {
        Pause,
        Create,
        Delete
    }
}
