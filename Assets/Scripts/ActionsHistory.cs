﻿using System.Collections.Generic;
using UnityEngine;

public class ActionsHistory
{
    public List<PlayerAction> data;
    public ActionsHistory()
    {
        data = new List<PlayerAction>();
    }
    public void Push(PlayerAction action)
    {
        data.Add(action);
        Debug.Log($"Pushing: {data.Count}");
    }

    public PlayerAction Pop()
    {
        if (data.Count <= 0) return null;
        var pa = data[data.Count - 1];
        data.Remove(pa);
        Debug.Log($"popping: {data.Count}");
        return pa;
    }
}
public abstract class PlayerAction
{
    public abstract void Execeute(Area area);
}

public class CreatePlayerAction : PlayerAction
{
    private Vector2Int position;
    public CreatePlayerAction(Vector2Int position)
    {
        this.position = position;
    }

    public override void Execeute(Area area)
    {
        area.Remove(position);
    }
}

public class ReplacePlayerAction : PlayerAction
{
    private Vector2Int position;
    private Vector3 oldPosition;
    private Vector3 oldRotation;
    private GameObject cubePrefab;
    public ReplacePlayerAction(Vector2Int position, Vector3 oldPosition, Vector3 oldRotation, GameObject cubePrefab)
    {
        this.position = position;
        this.oldPosition = oldPosition;
        this.oldRotation = oldRotation;
    }

    public override void Execeute(Area area)
    {
        area.Remove(position);
        var cube = area.CreateCube(position);
        cube.transform.position = oldPosition;
        cube.transform.eulerAngles = oldRotation;
    }
}