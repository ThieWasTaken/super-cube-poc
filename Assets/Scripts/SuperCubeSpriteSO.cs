﻿using UnityEngine;

[CreateAssetMenu(fileName = "new Super-Cube sprites", menuName = "Super-Cube/SpritesMap")]
public class SuperCubeSpriteSO : ScriptableObject
{
    public Sprite top, bottom, left, right, front, back;
}