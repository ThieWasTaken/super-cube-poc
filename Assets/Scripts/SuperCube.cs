using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SuperCube
{
    private Dictionary<FaceType, Face> faces;
    public Face surfaceFace;
    public SuperCube(Sprite topSprite, Sprite bottomSprite, Sprite rightSprite, Sprite leftSprite, Sprite frontSprite, Sprite backSprite)
    {
        faces = new Dictionary<FaceType, Face>();
        var allFaceType = Enum.GetValues(typeof(FaceType));
        
        for (int i = 0; i < allFaceType.Length; i++)
        {
            Sprite sprite = null;
            FaceType currentFaceType = FaceType.Back;

            switch (allFaceType.GetValue(i))
            {
                case FaceType.Top:
                    sprite = topSprite;
                    currentFaceType = FaceType.Top;
                    break;
                case FaceType.Bottom:
                    sprite = bottomSprite;
                    currentFaceType = FaceType.Bottom;
                    break;
                case FaceType.Right:
                    sprite = rightSprite;
                    currentFaceType = FaceType.Right;
                    break;
                case FaceType.Left:
                    sprite = leftSprite;
                    currentFaceType = FaceType.Left;
                    break;
                case FaceType.Front:
                    sprite = frontSprite;
                    currentFaceType = FaceType.Front;
                    break;
                case FaceType.Back:
                    sprite = backSprite;
                    currentFaceType = FaceType.Back;
                    break;
            }
            if (sprite != null)
            {
                var face = new Face(sprite, this, currentFaceType);
                faces.Add(currentFaceType, face);
            }
        }
        surfaceFace = faces[FaceType.Top];
        GenerateNeighbours();
    }
    private void GenerateNeighbours()
    {
        foreach (var f in faces)
        {
            switch (f.Key)
            {
                case FaceType.Top:

                    f.Value.AddNeighbour(CubeFaceRelation.Backward, faces[FaceType.Back]);
                    f.Value.AddNeighbour(CubeFaceRelation.Forward, faces[FaceType.Front]);
                    f.Value.AddNeighbour(CubeFaceRelation.Left, faces[FaceType.Right]);
                    f.Value.AddNeighbour(CubeFaceRelation.Right, faces[FaceType.Left]);
                    break;
                case FaceType.Bottom:
                    f.Value.AddNeighbour(CubeFaceRelation.Backward, faces[FaceType.Front]);
                    f.Value.AddNeighbour(CubeFaceRelation.Forward, faces[FaceType.Back]);
                    f.Value.AddNeighbour(CubeFaceRelation.Left, faces[FaceType.Right]);
                    f.Value.AddNeighbour(CubeFaceRelation.Right, faces[FaceType.Left]);
                    break;
                case FaceType.Right:
                    f.Value.AddNeighbour(CubeFaceRelation.Backward, faces[FaceType.Back]);
                    f.Value.AddNeighbour(CubeFaceRelation.Forward, faces[FaceType.Front]);
                    f.Value.AddNeighbour(CubeFaceRelation.Left, faces[FaceType.Bottom]);
                    f.Value.AddNeighbour(CubeFaceRelation.Right, faces[FaceType.Top]);
                    break;
                case FaceType.Left:
                    f.Value.AddNeighbour(CubeFaceRelation.Backward, faces[FaceType.Back]);
                    f.Value.AddNeighbour(CubeFaceRelation.Forward, faces[FaceType.Front]);
                    f.Value.AddNeighbour(CubeFaceRelation.Left, faces[FaceType.Top]);
                    f.Value.AddNeighbour(CubeFaceRelation.Right, faces[FaceType.Bottom]);
                    break;
                case FaceType.Front:
                    f.Value.AddNeighbour(CubeFaceRelation.Backward, faces[FaceType.Top]);
                    f.Value.AddNeighbour(CubeFaceRelation.Forward, faces[FaceType.Bottom]);
                    f.Value.AddNeighbour(CubeFaceRelation.Left, faces[FaceType.Right]);
                    f.Value.AddNeighbour(CubeFaceRelation.Right, faces[FaceType.Left]);
                    break;
                case FaceType.Back:
                    f.Value.AddNeighbour(CubeFaceRelation.Backward, faces[FaceType.Bottom]);
                    f.Value.AddNeighbour(CubeFaceRelation.Forward, faces[FaceType.Top]);
                    f.Value.AddNeighbour(CubeFaceRelation.Left, faces[FaceType.Right]);
                    f.Value.AddNeighbour(CubeFaceRelation.Right, faces[FaceType.Left]);
                    break;
            }
        }
    }

    public Face RotateRight()
    {
        surfaceFace = surfaceFace.neighbours[CubeFaceRelation.Right];
        return surfaceFace;
    }

    public Face RotateLeft()
    {
        surfaceFace = surfaceFace.neighbours[CubeFaceRelation.Left];
        return surfaceFace;
    }

    public Face RotateForward()
    {
        surfaceFace = surfaceFace.neighbours[CubeFaceRelation.Forward];
        return surfaceFace;
    }

    public Face RotateBackward()
    {
        surfaceFace = surfaceFace.neighbours[CubeFaceRelation.Backward];
        return surfaceFace;
    }
}

public class Face
{
    public SuperCube cube;
    private FaceType type;
    public Sprite sprite;
    public Dictionary<CubeFaceRelation, Face> neighbours;
    public Face(Sprite sprite, SuperCube cube, FaceType faceType)
    {
        this.sprite = sprite;
        this.cube = cube;
        this.type = faceType;
    }

    public void AddNeighbour(CubeFaceRelation neighbourType, Face neighbour)
    {
        if (neighbours == null) neighbours = new Dictionary<CubeFaceRelation, Face>();
        neighbours.Add(neighbourType, neighbour);
    }
}
public enum FaceType
{
    Top,
    Bottom,
    Right,
    Left,
    Front,
    Back
}

public enum CubeFaceRelation
{
    Right,
    Left,
    Forward,
    Backward
}