using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class CubeDisplayerUI : CubeDisplayer
{
    private Image imgComponent;
    protected override void OnEnable()
    {
        imgComponent = GetComponent<Image>();
        base.OnEnable();
    }

    protected override void UpdateDisplay()
    {
        imgComponent.sprite = cube.surfaceFace.sprite;
    }
}
