﻿using UnityEngine;

public abstract class CubeDisplayer : MonoBehaviour
{
    public SuperCube cube;
    public SuperCubeSpriteSO sprites;
    public Vector3 rotation;
    protected virtual void OnEnable()
    {
        cube = new SuperCube(sprites.top, sprites.bottom, sprites.right, sprites.left, sprites.front, sprites.back);
        rotation = Vector3Int.zero;
        UpdateDisplay();
    }

    public void RotateRight()
    {
        cube.RotateRight();
        transform.Rotate(new Vector3(0, 0, -90f), Space.World);
        UpdateDisplay();
    }
    public void RotateLeft()
    {
        cube.RotateLeft();
        transform.Rotate(new Vector3(0, 0, 90f), Space.World);
        UpdateDisplay();
    }
    public void RotateForward()
    {
        cube.RotateForward();
        transform.Rotate(new Vector3(90, 0, 0), Space.World);
        UpdateDisplay();
    }
    public void RotateBackward()
    {
        cube.RotateBackward();
        transform.Rotate(new Vector3(-90, 0, 0), Space.World);
        UpdateDisplay();
    }

    protected abstract void UpdateDisplay();
}